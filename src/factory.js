/* 工厂模式 */
// 工厂函数
function createPerson(name, age, profession) {
  // 创建新对象
  const person = {};

  // 设置属性和方法
  person.name = name;
  person.age = age;
  person.profession = profession;

  person.greet = function () {
    console.log(
      `Hello, my name is ${this.name}. I am ${this.age} years old. I work as a ${this.profession}.`
    );
  };

  // 返回新对象
  return person;
}

// 使用工厂函数创建对象
const person1 = createPerson("Alice", 25, "Engineer");
const person2 = createPerson("Bob", 30, "Designer");

person1.greet(); // 输出：Hello, my name is Alice. I am 25 years old. I work as a Engineer.
person2.greet(); // 输出：Hello, my name is Bob. I am 30 years old. I work as a Designer.
