// 原始对象
const realSubject = {
  request: function () {
    console.log("真实主题处理请求");
  },
};

// 代理对象
const proxy = {

  /* 设置代理的目标 */
  target:null,
  setTarget(tar){
    proxy.target = tar
  },

  /* 通过代理发起对目标的访问 */
  request: function (target) {
    // 代理记录了真身的访问日志
    console.log("代理对象处理请求");

    // 权限校验/访问控制 通知DOM更新/V3响应式 对参数做XSS校验 看看是否有结果缓存 图片懒加载 ...

    // 调用原始对象的方法
    proxy.target.request();
  },

};

// 使用代理对象
proxy.setTarget(realSubject)
proxy.request(); // 输出：代理对象处理请求 和 真实主题处理请求
