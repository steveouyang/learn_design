/* 观察者模式 observor */

// 主题（Subject）
class Subject {
  constructor() {
    // 将来会有一堆观察者
    this.observers = [];
  }

  /* 添加观察者到备胎列表 */
  addObserver(observer) {
    this.observers.push(observer);
  }

  /* 从备胎列表中移出观察者 */
  removeObserver(observer) {
    const index = this.observers.indexOf(observer);
    if (index !== -1) {
      this.observers.splice(index, 1);
    }
  }

  notify(message) {
    /* 通知所有的观察者都来响应 */
    this.observers.forEach(
      (observer) => observer.onMessage(message)
    );
    
  }
}

// 观察者（Observer）
class Observer {
  constructor(name) {
    this.name = name;
  }

  onMessage(message) {
    console.log(this.name, "收到通知/onMessage", message);
  }
  
}

// 示例用法
// 一个主题
const subject = new Subject();

// 两个观察者
const observer1 = new Observer("张三");
const observer2 = new Observer("蔡某");

// 主题添加2名观察者
subject.addObserver(observer1);
subject.addObserver(observer2);
// subject.removeObserver(observer2)

// 主题有变化时通知观察者
subject.notify("Hello, observers!");
