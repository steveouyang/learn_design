// 代理对象
class LazyImg {
  constructor(url) {
    this.url = url;

    // 要代理的真身
    this.image = null;
  }

  start() {
    /* 先创建一个真身图片 将图片地址塞过去 */
    if (!this.image) {
      this.image = new Image(400);
      //this.image.src = this.url;

      /* ========代理该做的一整套懒加载动作======== */
      // 真身先显示默认图片
      this.image.src = "./imgs/default.webp";

      // 创建代理图片去加载大图
      const proxy = new Image();
      proxy.src = this.url; //加载大图
      proxy.onload = (e) => {
        setTimeout(() => {
          // 代理加载完大图后 替换真身的src为大图
          this.image.src = this.url;
        }, 3000);
      };
      /* ======================================== */
    }

    // 返回真身
    return this.image;
  }
}

// 示例用法
document.body.appendChild(
  new LazyImg(
    "https://img1.baidu.com/it/u=2504433047,2178696880&fm=253&fmt=auto&app=120&f=JPEG?w=1422&h=800"
  ).start()
);
